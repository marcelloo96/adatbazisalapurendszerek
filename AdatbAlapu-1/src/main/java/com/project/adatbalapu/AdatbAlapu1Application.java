package com.project.adatbalapu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdatbAlapu1Application {

	public static void main(String[] args) {
		SpringApplication.run(AdatbAlapu1Application.class, args);
	}
}
